FROM node:current-alpine AS build
ADD ./ /project
WORKDIR /project
RUN npm install \
  && npm run lint \
  && npx quasar build -m pwa

FROM nginx:alpine AS prod
COPY docker/nginx-service.conf /etc/nginx/conf.d/default.conf
RUN rm -fr /usr/share/nginx/html
COPY --from=build /project/dist/pwa /usr/share/nginx/html
