import type { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  { path: '/', redirect: '/lists' },
  {
    path: '/settings',
    component: () => import('layouts/SimpleLayout.vue'),
    children: [{ path: '', component: () => import('pages/SettingsPage.vue') }]
  },
  {
    path: '/lists',
    component: () => import('layouts/SimpleLayout.vue'),
    children: [{ path: '', component: () => import('pages/ListsPage.vue') }]
  },
  {
    path: '/list',
    props: true,
    component: () => import('layouts/CategoriesDrawerLayout.vue'),
    children: [
      { path: ':listId/items', props: true, component: () => import('pages/ItemsPage.vue') },
      { path: ':listId/:categoryId/items', props: true, component: () => import('pages/ItemsPage.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
