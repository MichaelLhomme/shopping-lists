interface JsonItem {
  _id: string
  data: object
}

type JsonAccumulator = { lists: JsonItem[], categories: JsonItem[], items: JsonItem[] }

export function exportContent(db: PouchDB.Database): Promise<string> {
  return db
    .allDocs({include_docs: true})
    .then(res => res.rows.map(row => row.doc!))
    .then(res => JSON.stringify(res, null, 2))
}

export function importContent(db: PouchDB.Database, content: string): Promise<void> {
  return Promise
    .resolve(content)
    .then(payload => {
      const json = JSON.parse(payload)
      if(!Array.isArray(json)) throw new Error('Content is not an array')
      const arr = json as JsonItem[]
      return arr.map(item => ({ _id: item._id, data: item.data } as JsonItem))
    })
    .then(json => json.reduce<JsonAccumulator>((acc, item) => {
      const { lists, categories, items } = acc
      
      if(item._id.startsWith('list'))
        return { lists: [...lists, item], categories, items }
      else if(item._id.startsWith('category'))
        return { lists, categories: [...categories, item], items }
      else if(item._id.startsWith('item'))
        return { lists, categories, items: [...items, item] }
      else {
        console.error('Unknown item', item)
        return acc
      }
    }, {lists: [], categories: [], items: []}))
    .then(({ lists, categories, items }) => [...lists, ...categories, ...items])
    .then(res => {
        console.log('Importing', res)
        return db.bulkDocs(res)
    })
    .then(_docs => void 0) // discard documents for caller
}