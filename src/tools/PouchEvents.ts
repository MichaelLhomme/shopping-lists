export enum PouchEventType {
  CREATED = 'created',
  UPDATED = 'updated',
  DELETED = 'deleted'
}

export interface PouchEvent<T> {
  type: PouchEventType
  data: T
}
