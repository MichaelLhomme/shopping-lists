import { Notify, Dialog } from 'quasar'
import ErrorPopup from 'components/ErrorPopup.vue'

export function notifySuccess(msg: string): void {
  Notify.create({
    message: msg,
    color: 'positive',
    icon: 'done',
    timeout: 1000,
    progress: true,
  })
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function notifyError(msg: string, err: any): void {
  Notify.create({
    message: `${msg}: ${err.message ?? err}`,
    color: 'negative',
    timeout: 3000,
    progress: true,
    actions: [
      {
        icon: 'info',
        color: 'white',
        handler: () => {
          Dialog.create({
            component: ErrorPopup,
            componentProps: { err }
          })
        }
      }
    ]
  })
}
