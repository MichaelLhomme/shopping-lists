import PouchDB from 'pouchdb'
import find from 'pouchdb-find'
import rel from 'relational-pouch'
import { Subject } from 'rxjs'

import { PouchEventType } from './PouchEvents'
import { Entities, Schema } from './PouchSchema'

import type { Observable } from 'rxjs'
import type { EntityBase } from 'src/models/base'
import type { PouchEvent } from './PouchEvents'
import { notifyError, notifySuccess } from './Notify'


const LOCAL_STORAGE_KEY = 'remote-url'

export enum DatabaseEvent {
  DATABASE_OPENED,
  DATABASE_SYNC_UPDATE,
  REPLICATION_SETUP,
  REPLICATION_STOPPED
}


export class PouchConnection {
  // Internal DB reference
  private db: PouchDB.Database

  // Internal relational DB reference
  private relDb: PouchDB.RelDatabase
  
  // Internal change listener reference
  private changeListener: PouchDB.Core.Changes<object>

  // Internal map of RXJS subjects for events related to documents
  private changes = {} as Record<Entities, Subject<PouchEvent<unknown>>>
  
  private events: Subject<DatabaseEvent>

  // Reference to a remote database for replication
  private remoteDb: PouchDB.Database | undefined
  private syncHandler: PouchDB.Replication.Sync<object> | undefined


  /** Constructor */
  constructor() {
    PouchDB.plugin(find).plugin(rel)

    // Init event dispatchers
    this.events = new Subject<DatabaseEvent>()
    this.changes[Entities.List] = new Subject<PouchEvent<unknown>>()
    this.changes[Entities.Category] = new Subject<PouchEvent<unknown>>()
    this.changes[Entities.Item] = new Subject<PouchEvent<unknown>>()

    // Init database
    const { db, relDb, changeListener } = this.initDatabase()
    this.db = db
    this.relDb = relDb
    this.changeListener = changeListener
    this.events.next(DatabaseEvent.DATABASE_OPENED)

    // Auto restart replication if available
    this.autoRestartReplication()
  }

  getDb(): PouchDB.Database {
    return this.db
  }

  getRelDb(): PouchDB.RelDatabase {
    return this.relDb
  }
  
  getRemoteDb(): PouchDB.Database | undefined {
    return this.remoteDb
  }
  
  getEvents(): Observable<DatabaseEvent> {
    return this.events.asObservable()
  }

  getChanges<T>(type: Entities): Observable<PouchEvent<T>> {
    return this.changes[type].asObservable() as Observable<PouchEvent<T>>
  }

  parseDocID(docId: string): { id: string; type: Entities } {
    const { id, type } = this.relDb.rel.parseDocID(docId)
    if (!Object.values(Entities).includes(type)) throw new Error(`Unknwon entity type ${type}`)

    return { id, type }
  }

  getItems<T>(type: Entities): Promise<T[]> {
    return this.relDb.rel.find(type).then(res => res[Schema[type].plural])
  }

  createItem<T>(type: Entities, item: T): Promise<T> {
    return this.relDb.rel.save(type, item).then(res => ({ ...item, rev: res.rev }))
  }

  updateItem<T>(type: Entities, list: T): Promise<T> {
    return this.relDb.rel.save(type, list).then(res => ({ ...list, rev: res.rev }))
  }

  deleteItem<T>(type: Entities, list: T): Promise<{ deleted: boolean }> {
    return this.relDb.rel.del(type, list)
  }
 
  recreateDatabase(): Promise<void> {
    return this.relDb
      .destroy()
      .then(() => {
        this.changeListener.cancel()

        const { db, relDb, changeListener } = this.initDatabase()
        this.db = db
        this.relDb = relDb
        this.changeListener = changeListener
        this.events.next(DatabaseEvent.DATABASE_OPENED)
      })
  }

  replicate(url: string): Promise<void> {
    if(this.remoteDb !== undefined) throw new Error('Replication is already setup')    
    return this
      .setupReplication(url)
      .then(() => this.events.next(DatabaseEvent.REPLICATION_SETUP))
  }
 
  stopReplication() {
    if(this.syncHandler === undefined) return
    this.syncHandler.cancel()
    this.syncHandler = undefined
    this.remoteDb = undefined
    localStorage.removeItem(LOCAL_STORAGE_KEY)
    this.events.next(DatabaseEvent.REPLICATION_STOPPED)
  }
  
  private initDatabase(): { db: PouchDB.Database, relDb: PouchDB.RelDatabase, changeListener: PouchDB.Core.Changes<object> } {
    const db = new PouchDB('shopping-lists')
    const relDb = this.setSchema(db)
    const changeListener = this.listenChanges(relDb)
    return { db, relDb, changeListener }
  }

  private setSchema(db: PouchDB.Database): PouchDB.RelDatabase {
    return db.setSchema(Object.values(Schema))
  }

  private listenChanges<T extends EntityBase>(relDb: PouchDB.RelDatabase): PouchDB.Core.Changes<object> {
    return relDb
      .changes({
        since: 'now',
        live: true,
        include_docs: true
      })
      .on('error', error => console.error('Change listener error', error))
      .on('change', change => {
        const { id, type: entityType } = relDb.rel.parseDocID(change.id)
        //console.log('onchange', entityType, id, change)

        if (change.doc === undefined) throw new Error('Missing doc in event')

        if (!Object.values(Entities).includes(entityType)) throw new Error(`Unknwon entity type ${entityType}`)

        const event = {} as PouchEvent<T>
        const doc = (change.doc as unknown as { data: T }).data // TODO typing problem, caused by include_docs ?
        event.data = {
          ...doc,
          id: id,
          rev: change.doc._rev
        }

        if (change.deleted === true) {
          event.type = PouchEventType.DELETED
        } else if (change.doc._rev.startsWith('1-')) {
          event.type = PouchEventType.CREATED
        } else {
          event.type = PouchEventType.UPDATED
        }

        this.changes[entityType as Entities].next(event)
      })
  }
  
  private autoRestartReplication(): void {
    const remoteUrl = localStorage.getItem(LOCAL_STORAGE_KEY)
    if(remoteUrl) {
      this
        .replicate(remoteUrl)
        .then(() => notifySuccess('Réplication configurée'))
        .catch(err => notifyError('Erreur lors de la configuration de la réplication', err))
    }
  }

  private setupReplication(url: string): Promise<void> {
    return navigator
      .credentials
      .get({ password: true } as CredentialRequestOptions)
      .then(cred => cred ? cred : Promise.reject("Impossible de récupérer les identifiants"))
      .then((cred: any) => ({ username: cred.name, password: cred.password })) // eslint-disable-line @typescript-eslint/no-explicit-any
      .then(({ username, password }) => new PouchDB(url, {
        skip_setup: true,
        auth: { username, password }
      }))
      .then(db => db
        .info()
        .then(info => ({ db, info }))
      )
      .then(({ db, info }) => {
        const res: any = info // eslint-disable-line @typescript-eslint/no-explicit-any
        if(res.error !== undefined) throw new Error(res.reason)
        return db
      })
      .then(db => {
        // Save in localStorage
        localStorage.setItem(LOCAL_STORAGE_KEY, url)

        // Setup sync
        this.remoteDb = db
        this.syncHandler = this.db.sync(this.remoteDb, {
          live: true,
          retry: true
        }) 
       
        const connEvents = this.events;
        this.syncHandler
          .on('change', function (change) {
            if(change.direction === 'pull') {
              connEvents.next(DatabaseEvent.DATABASE_SYNC_UPDATE)
            }
          })
          .on('paused', function (info) {
            // replication was paused, usually because of a lost connection
            console.warn("Replication paused", info)
          })
          .on('error', function (err) {
            // totally unhandled error (shouldn't happen)
            console.warn("Replication error", err)
          })
          .on('denied', function (err) {
            // permission error
            console.warn("Replication was denied !", err)
          })
          .on('active', function () {
            // replication was resumed
            console.warn("Replication is active !")
          })
          .on('complete', function (info) {
            // replication was cancelled
            console.warn("Replication stopped !", info)
          });
      })
  }

}

// Exports
let _connection: PouchConnection
export const connection = (): PouchConnection => {
  if (_connection === undefined) _connection = new PouchConnection()
  return _connection
}

export const testRemoteConnection = (url: string, username: string, password: string): Promise<boolean> => {
  const testDb = new PouchDB(url, {
    skip_setup: true,
    auth: { username, password }
  })

  return testDb
    .info()
    .then(info => {
      const res: any = info // eslint-disable-line @typescript-eslint/no-explicit-any
      if(res.error !== undefined) throw new Error(res.reason)
      return true
    })
}
