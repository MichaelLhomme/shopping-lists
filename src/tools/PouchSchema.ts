export enum Entities {
  List = 'list',
  Category = 'category',
  Item = 'item'
}

export const Schema = {
  [Entities.List]: {
    singular: Entities.List,
    plural: 'lists',
    relations: {
      categories: { hasMany: Entities.Category }
    }
  },
  [Entities.Category]: {
    singular: Entities.Category,
    plural: 'categories',
    relations: {
      list: { belongsTo: Entities.List },
      items: { hasMany: Entities.Item }
    }
  },
  [Entities.Item]: {
    singular: Entities.Item,
    plural: 'items',
    relations: {
      list: { belongsTo: Entities.List },
      category: { belongsTo: Entities.Category }
    }
  }
}
