export type EntityID = string | number
export interface EntityBase {
  id: EntityID
}
