import type { EntityBase } from './base'
import type { ListID } from './list'

export type CategoryID = string
export interface Category extends EntityBase {
  id: CategoryID
  name: string
  color: string
  list: ListID
}
