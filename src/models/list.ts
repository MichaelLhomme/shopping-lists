import type { EntityBase } from './base'

export type ListID = string
export interface List extends EntityBase {
  id: ListID
  name: string
  description: string
}
