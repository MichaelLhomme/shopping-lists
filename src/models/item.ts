import type { EntityBase } from './base'
import type { CategoryID } from './category'
import type { ListID } from './list'

export type ItemID = string
export interface Item extends EntityBase {
  id: ItemID
  name: string
  checked: boolean
  list: ListID
  category: CategoryID | undefined
}
