import { computed, ref } from 'vue'

import { connection } from 'src/tools/PouchConnection'
import { PouchEventType } from 'src/tools/PouchEvents'
import { notifyError } from 'src/tools/Notify'

import type { EntityBase, EntityID } from 'src/models/base'
import type { Entities } from 'src/tools/PouchSchema'
import type { Ref } from 'vue'

export function useAbstractStore<T extends EntityBase>(type: Entities) {
  const conn = connection()
  const items = ref([]) as Ref<T[]>

  const createItem = (item: T) => conn
    .createItem<T>(type, item)
    .catch(err => notifyError("Erreur lors de la création d'un élément", err))

  const updateItem = (item: T) => conn
    .updateItem<T>(type, item)
    .catch(err => notifyError("Erreur lors de la mise à jour d'un élément", err))

  const deleteItem = (item: T) => conn
    .deleteItem<T>(type, item)
    .catch(err => notifyError("Erreur lors de la suppression", err))


  // Initial loading
  const initialLoad = () => conn
    .getItems<T>(type)
    .then(res => (items.value = res || []))
    .catch(err => notifyError("Erreur lors de la récupération des éléments", err))

  // Items by ids
  const itemsByIds = computed(() =>
    items.value.reduce<Record<EntityID, T>>((acc, item) => {
      acc[item.id] = item
      return acc
    }, {})
  )

  // Listen to changes
  conn.getChanges<T>(type).subscribe(event => {
    //console.log(`received change for ${type}`, event, items, items.value);
    const entity = event.data
    switch (event.type) {
      case PouchEventType.CREATED:
        items.value.push(entity)
        break

      case PouchEventType.UPDATED: {
        const i = items.value.findIndex(l => l.id === entity.id)
        if (i < 0) throw new Error(`${type} not found '${entity.id}'`)
        items.value[i] = entity
        break
      }

      case PouchEventType.DELETED: {
        const j = items.value.findIndex(l => l.id === entity.id)
        if (j < 0) throw new Error(`${type} not found '${entity.id}'`)
        items.value.splice(j, 1)
        break
      }

      default:
        console.error(`No such PouchEventType ${event.type}`)
        break
    }
  })

  return {
    conn,
    items,
    itemsByIds,
    initialLoad,
    createItem,
    updateItem,
    deleteItem
  }
}
