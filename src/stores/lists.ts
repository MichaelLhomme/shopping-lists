import { defineStore } from 'pinia'

import { Entities } from 'src/tools/PouchSchema'
import { useAbstractStore } from './abstract-store'

import type { List } from 'src/models/list'

export const useListsStore = defineStore('lists', () => {
  const { items, initialLoad, createItem, updateItem, deleteItem } = useAbstractStore<List>(Entities.List)

  initialLoad()

  return {
    lists: items,
    createList: createItem,
    updateList: updateItem,
    deleteList: deleteItem
  }
})
