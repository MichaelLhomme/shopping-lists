import { defineStore } from 'pinia'
import { Entities, Schema } from 'src/tools/PouchSchema'

import { useAbstractStore } from './abstract-store'
import { notifyError } from 'src/tools/Notify'

import type { Category } from 'src/models/category'
import type { ListID } from 'src/models/list'

export const useCategoriesStore = defineStore('categories', () => {
  const { conn, items, itemsByIds, createItem, updateItem, deleteItem } = useAbstractStore<Category>(Entities.Category)

  const initForList = (listId: ListID) => {
    items.value = []
    conn
      .getRelDb()
      .rel.findHasMany(Entities.Category, Schema[Entities.Category].relations.list.belongsTo, listId)
      .then(res => {
        items.value = res[Schema[Entities.Category].plural]
      })
      .catch(err => notifyError("Impossible de lister les catégories", err))
  }

  return {
    categories: items,
    categoriesByIds: itemsByIds,
    createCategory: createItem,
    updateCategory: updateItem,
    deleteCategory: deleteItem,
    initForList
  }
})
