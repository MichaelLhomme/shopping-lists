import { defineStore } from 'pinia'
import { Entities, Schema } from 'src/tools/PouchSchema'
import { computed, ref } from 'vue'

import { useAbstractStore } from './abstract-store'
import { notifyError } from 'src/tools/Notify'

import type { CategoryID } from 'src/models/category'
import type { Item } from 'src/models/item'
import type { ListID } from 'src/models/list'

export const useItemsStore = defineStore('items', () => {
  const { conn, items, itemsByIds, createItem, updateItem, deleteItem } = useAbstractStore<Item>(Entities.Item)

  const initForList = (listId: ListID) => {
    items.value = []
    conn
      .getRelDb()
      .rel.findHasMany(Entities.Item, Schema[Entities.Item].relations.list.belongsTo, listId)
      .then(res => {
        items.value = res[Schema[Entities.Item].plural]
      })
      .catch(err => notifyError("Impossible de lister les articles", err))
  }

  const category = ref()
  const filterByCategory = (categoryId?: CategoryID) => (category.value = categoryId)

  const filteredItems = computed(() => {
    if (category.value === undefined) return items.value
    return items.value.filter(i => i.category === category.value)
  })

  return {
    items: filteredItems,
    itemsByIds,
    createItem,
    updateItem,
    deleteItem,
    initForList,
    filterByCategory
  }
})
